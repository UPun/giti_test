# README #

Test, mis kuulub hiljem kustutamisele

### IntelliJ IDEA git-i seadistuse testimine ###

* Üritan selgeks saada, kuidas toimub IntelliJ IDEA abil oma muudatuste ja ühistöö üleslaadimine. Et see toimiks kinnisilmi.
* Version 1
*Ära mine närvi, ära mine närvi, närv pole pood, kuhu peab minema (Metsakutsu)
### Lihtsalt testin ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

VCS->Git->Commit File...
VCS->Git->Remotes...->
Siin saan konfida oma bitbucketi konto asukoha
VCS->Git->Push saadan oma muudatused bitbucketisse
VCS->Git->Pull tõmban bitbucketist, kui ma olen vahepeal seal committinud/muudatusi teinud.
git init
git status
git add faili-nimi.txt
Teen muudatused oma töös, seejärel:
git diff
git add faili-nimi.txt
git commit -m "Tekst muutuste kohta"
Võib ka lihtsalt git commit
git log
--
git show HEAD
head commit faili-nimi.txt
git checkout HEAD changes.txt
Discard changes in the working directory.

git diff
git checkout faili-nimi.txt
Kui mitu faili, siis
git add fail1.txt fail2.txt

git reset HEAD filename
Unstages file changes in the staging area.
git reset SHA
SHA -> esimesed 7 tähemärki (vt. git log)
Can be used to reset to a previous commit in your commit history.

Check what branch you are currently on:
git branch
git branch new_branch

### Contribution guidelines ###

### Üks väga hea giti õpetus: http://rogerdudler.github.io/git-guide/ 
Soovitan algatuseks läbi teha koodiakadeemiast selle õpetuse: https://www.codecademy.com/learn/learn-git
###

* Repo owner UPu*